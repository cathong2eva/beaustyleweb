$(function() {
  /* Redirect for navigation */
  if ($('.main-navigation').length) {
    $('.main-navigation input[type=radio]').click(function(e) {
      e.preventDefault();

      window.location.replace($('.main-navigation input:checked').val());
    });
  }

  /* Search result page */
  // if ($('.tag_new .tag_delete').length) {
  //   $('.tag_new .tag_delete').click(function(e) {
  //     e.preventDefault();
  //     var tag = $(this).data('tag');

  //     window.location.replace(removeURLParameterByValue(tag, decodeURIComponent(window.location.href)));
  //   });
  // }

  if ($('.search_button_new').length) {
    $('.search_button_new select').change(function() {
      window.location.replace($(this).find('option:selected').data('url'));
    });
  }

  /* Scroll to top: aタグを押した際のページ内遷移処理 */
  if ($('a[href^="top"]').length) {
    var $goTop = $('a[href^="top"]');
    $(window).scroll(function() {
      if ($(this).scrollTop() > 100) {
        $('.footer_gotop').fadeIn();
      } else {
        $('.footer_gotop').fadeOut(2000);
      }
    });

    $goTop.click(function() {
      var speed = 400;
      var href= $(this).attr('href');
      var target = $(href == 'top' || href == '' ? 'html' : href);
      var position = target.offset().top;

      $('html, body').animate({ scrollTop: position }, speed, 'swing');
      return false;
    });
  }

  /* pagination */
  loadMore = function(heightFooter) {
    $('.wrapper_posts').infinitescroll({
      navSelector: '#pagination',
      nextSelector: '#pagination a',
      itemSelector: '.post',
      dataType: 'html',
      pixelsFromNavToBottom: heightFooter,

      // animate: true,
      loading: {
        img: '/img/loading.gif',
        msgText: 'ロードしています...', // ローディング中に出すテキスト
        finishedMsg: '' // ローディング終了時に出すテキスト
      }
    }, function(html, opts) {
      if (html.length < $(this).data('limit')) { /* { limit: 20, banner: 4 } */
        opts.state.isDone = true;
      }
    });
  };

  bindScroll = function() {
    var heightFooter = $('footer').height() - 50; //console.log(heightFooter);
    if ($(document).height() - $(window).height() - $(window).scrollTop() < heightFooter ) {
      loadMore(heightFooter);
    }
  }

  $(window).scroll(bindScroll);

  $('body').on('click', '.image-banner', function(e) {
    var objectId = $(this).attr("data-banner-id");
    $.ajax({
      type: 'POST',
      data: { id: objectId },
      dataType: 'json',
      url: '/updateViewBanner',
      beforeSend: function(jqXHR, settings) {
      },
      success: function(data) {
      }
    });
  });

    $(window).scroll(function(){
      var heightScrollTop = $(this).scrollTop();
      var heightHead = $('header').height();
      var heightShoeHead = $('.header-shoedinate').height();
      if(heightScrollTop > (heightHead - heightShoeHead)) {
        $(".header-shoedinate").css("position", "fixed");
        $(".header-shoedinate").css("margin-top", -(heightHead));
      } else {
        $(".header-shoedinate").css("position", "static");
        $(".header-shoedinate").css("margin-top", 0);
      }
    });

// header esp-smt
    $('.accordion').click(function() {
      $(this).find('.hidden_box').toggle();
    });

    $('.menu_fixed').click(function() {
      $('#navi_container').css('display', 'block');
      $('.header-shoedinate').css('display', 'none');
    });

    $('.navi-close').click(function() {
      $('#navi_container').css('display', 'none');
      $('.header-shoedinate').css('display', 'block');
    });

});

var removeURLParameterByValue = function(paramValue, sourceURL) {
  var urlReturn = sourceURL.split('?')[0],
      param,
      params = [],
      queryString = (sourceURL.indexOf('?') !== -1) ? sourceURL.split('?')[1] : '';

  if (queryString !== '') {
    params = queryString.split('&');

    for (var i = params.length - 1; i >= 0; i -= 1) {
      param = params[i].split('=');

      if (typeof param[1] != 'undefined' && param[1] === paramValue) {
        params.splice(i, 1);
      }
    }

    if (params.length) {
      urlReturn = urlReturn + '?' + params.join('&');
    }
  }

  return urlReturn;
}

var voteFavorite = function(obj) {
  var $me = $(obj);
  var iconFavorite = $me.data('icon-favorite'); // /img/favorites/favorite.png
  var iconUnFavorite = $me.data('icon-unfavorite'); // /img/home/favorite.png
  var point = ($me.find('img').attr('src') == iconFavorite) ? -1 : 1;

  $.ajax({
    type: 'POST',
    data: { id: $me.data('post-id'), point: point },
    dataType: 'json',
    url: '/like',
    beforeSend: function(jqXHR, settings) {
    },
    success: function(data) {
      if (data.success) {
        if (point == -1) {
          $me.find('img').attr('src', iconUnFavorite);
        } else {
          $me.find('img').attr('src', iconFavorite);
        }

        if ($me.next('.like_count_detail').length) {
          $me.next('.like_count_detail').text(parseInt($me.next('.like_count_detail').text()) + point);
        }

        // Show or hide icon favorite on header
        var $headerFavorite = $('header .favorite');
        $headerFavorite.data('num-favorite', $headerFavorite.data('num-favorite') + point);
        if ($headerFavorite.data('num-favorite') > 0) {
          $headerFavorite.removeClass('active');
          $headerFavorite.attr('href', $headerFavorite.data('url'));
        } else {
          $headerFavorite.attr('href', 'javascript:;');
          $headerFavorite.addClass('active');

          // Redirect to home if cookies is empty
          if (window.location.href.indexOf('/favorites') > 0) {
            window.location.replace('/');
          }
        }
      }
    }
  });
}

var showModal = function(modalId) {
  if ($('#' + modalId).css('display') == 'none') {
    $('.modal').css('display', 'none');
    $('#' + modalId).css('display', 'block');
  } else {
    $('#' + modalId).css('display', 'none');
  }
};
var hideModal = function() {
  $('.modal').css('display', 'none');
};


(function() {
  /*
   * Is property defined?
   */
  function isDefined(property) {
    // workaround https://github.com/douglascrockford/JSLint/commit/24f63ada2f9d7ad65afc90e6d949f631935c2480
    var propertyType = typeof property;

    return propertyType !== 'undefined';
  }

  /*
   * Extract parameter from URL
   */
  function getParameter(url, name) {
    var regexSearch = "[\\?&#]" + name + "=([^&#]*)";
    var regex = new RegExp(regexSearch);
    var results = regex.exec(url);
    return results ? window.decodeURIComponent(results[1]) : '';
  }

  /*
   * Set cookie value
   */
  function setCookie(cookieName, value, msToExpire, path, domain, secure) {
    var expiryDate;

    // relative time to expire in milliseconds
    if (msToExpire) {
      expiryDate = new Date();
      expiryDate.setTime(expiryDate.getTime() + msToExpire);
    }

    document.cookie = cookieName + '=' + window.encodeURIComponent(value) +
      (msToExpire ? ';expires=' + expiryDate.toGMTString() : '') +
      ';path=' + (path || '/') +
      (domain ? ';domain=' + domain : '') +
      (secure ? ';secure' : '');
  }

  /*
   * Get cookie value
   */
  function getCookie(cookieName) {
    var cookiePattern = new RegExp('(^|;)[ ]*' + cookieName + '=([^;]*)'),
        cookieMatch = cookiePattern.exec(document.cookie);

    return cookieMatch ? window.decodeURIComponent(cookieMatch[2]) : 0;
  }

  /*
   * Delete cookie value
   */
  function deleteCookie(cookieName, path, domain) {
    setCookie(cookieName, '', -86400, path, domain);
  }

  /**
   * getParameters
   * @param {String} url
   * @return {Object}
   */
  function getParameters(url) {
    var campaign = getParameter(url, 'utm_campaign');
    if (campaign != null) {
      campaign = window.encodeURIComponent(campaign);
    }

    var source = getParameter(url, 'utm_source');
    if (source != null) {
      source = window.encodeURIComponent(source);
    }

    var medium = getParameter(url, 'utm_medium');
    if (medium != null) {
      medium = window.encodeURIComponent(medium);
    }

    var term = getParameter(url, 'utm_term');
    if (term != null) {
      term = window.encodeURIComponent(term);
    }

    var content = getParameter(url, 'utm_content');
    if (content != null) {
      content = window.encodeURIComponent(content);
    }

    return {
      id: campaign,
      source: source,
      medium: medium,
      term: term,
      productId: content
    };
  }

  /**
   * buildRequest
   * @param {Object} params
   * @return {String}
   */
  function buildRequest(params) {
    var queryString = '';
    var i = 0;
    for (property in params) {
      if (i > 0) {
        queryString += '&';
      }
      queryString += property + '=' + params[property];

      i++;
    }

    return queryString;
  }

  /**
   * initShoeCookie
   * @return {Void}
   */
  function initShoeCookie() {
    var params = getParameters(window.location.href); // document.URL
    if (isDefined(params['source']) && params['source'] == 'shoedinate' &&
      isDefined(params['term']) && params['term'] == 'shoedinate_service'
    ) {
      setCookie('shoedinate_item', buildRequest(params), 7*24*60*60*1000);
    }
  }

  function getConversionDomain() {
    return isDefined(shoedinateDomain) ? shoedinateDomain : '';
  }

  function getConversionURL() {
    return ('https:' == document.location.protocol ? 'https://' : 'http://') + getConversionDomain() + '/conversion';
  }

  function getLatestPostsURL() {
    return ('https:' == document.location.protocol ? 'https://' : 'http://') + getConversionDomain() + '/posts/latest';
  }

  function getRelatedPostsURL() {
    return ('https:' == document.location.protocol ? 'https://' : 'http://') + getConversionDomain() + '/posts/related';
  }

  /*
   * POST request to server using XMLHttpRequest.
   */
  function sendXmlHttpRequest(url, request, callback) {
    try {
      // we use the progid Microsoft.XMLHTTP because
      // IE5.5 included MSXML 2.5; the progid MSXML2.XMLHTTP
      // is pinned to MSXML2.XMLHTTP.3.0
      var xhr = window.XMLHttpRequest
        ? new window.XMLHttpRequest()
        : window.ActiveXObject
        ? new ActiveXObject('Microsoft.XMLHTTP')
        : null;

      // fallback on error
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
          try {
            if (typeof callback === 'function') {
              callback(this.responseText);
            } else {
              var responseObj = JSON.parse(this.responseText);
              if (responseObj.success) {
                deleteCookie('shoedinate_item');
              }
            }
          } catch (e) {
          }
        }
      };

      xhr.open('POST', url, true);

      xhr.upload.onprogress = function(e) {
        if (e.lengthComputable) {
          // console.log('Waiting for loading posts ...');

          // Check exist loading container
          if (document.getElementById('shoedinate-loading') != null) {
            document.getElementById('shoedinate-loading').style.display = 'block';
          }
        }
      }

      xhr.upload.onloadend = function(e) {
        // console.log('Loading posts finished.');

        // Check exist loading container
        if (document.getElementById('shoedinate-loading') != null) {
          document.getElementById('shoedinate-loading').style.display = 'none';
        }
      }

      // @link: http://stackoverflow.com/questions/1268673/set-a-request-header-in-javascript?answertab=active#tab-top
      // Set header so the called script knows that it's an XMLHttpRequest
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr.setRequestHeader('Content-Type', 'application/json');

      xhr.send(request);
    } catch (e) {
      // fallback
      console.log(e);
    }
  }

  /**
   * purchaseConfirm
   * @return {Void}
   */
  function purchaseConfirm() {
    if (window.location.href.indexOf('order/complete') > -1) {
      var request = getCookie('shoedinate_item');

      if (request != 0) {
        // Convert to json data
        // request = JSON.stringify(
        //   JSON.parse('{"' + request.replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
        // );
        var requestJSON = JSON.parse('{"' + request.replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
        var productId = requestJSON.productId;
        var requestString = JSON.stringify(requestJSON);


        if (document.getElementsByClassName("cart_complete")[0].getElementsByClassName("item_photo") != null &&
          document.getElementsByClassName("cart_complete")[0].getElementsByClassName("item_photo").length > 0
        ) {
          var hasProduct = false;
          var item_photos = document.getElementsByClassName("cart_complete")[0].getElementsByClassName("item_photo");

          for (var i = 0; i < item_photos.length; i++) {
            if (item_photos[i].getElementsByTagName("img")[0].src.indexOf(productId) > -1) {
              hasProduct = true;
              break;
            }
          }

          if (hasProduct === true) {
            sendXmlHttpRequest(getConversionURL(), requestString, null);
          }
        }
      }
    }
  }

  function getLatestPosts() {
    if (document.getElementById('shoedinate-latest') != null) {
      sendXmlHttpRequest(getLatestPostsURL(), JSON.stringify({ source: 'shoedinate', sp: 0 }), function(htmlOfList) {
        document.getElementById('shoedinate-latest').getElementsByTagName('ul')[0].innerHTML = htmlOfList;

        if (typeof shoedinateSlider !== 'undefined' && typeof shoedinateSlider.reloadSlider === 'function') {
          shoedinateSlider.reloadSlider();
        }
      });
    } else if (document.getElementById('shoedinate-latest-sp') != null) {
      sendXmlHttpRequest(getLatestPostsURL(), JSON.stringify({ source: 'shoedinate', sp: 1 }), function(htmlOfList) {
        document.getElementById('shoedinate-latest-sp').innerHTML = htmlOfList;
      });
    }
  }

  function getRelatedPosts() {
    if (document.getElementById('shoedinate-related') != null) {
      var shoedinateRelated = document.getElementById('shoedinate-related');
      getViewRelatedPosts(shoedinateRelated);
    } else if (document.getElementById('shoedinate-related-sp') != null){
      var shoedinateRelated = document.getElementById('shoedinate-related-sp');
      getViewRelatedPosts(shoedinateRelated);
    }
  }

  function getViewRelatedPosts(shoedinateRelated) {
    var productCds = document.getElementsByName('srDispProductDetailForm');
    var productCd = "";
    if (typeof(productCds) != 'undefined' && productCds != null && productCds.length > 0)
    {
      productCds = document.getElementsByName('srDispProductDetailForm')[0].elements;
      productCd = productCds['productCd'].value
    } else {
      productCds = document.getElementsByClassName("item_detail_info_code");
      var code = productCds[0].innerText.trim();
      productCd = code.substring(3, code.length);
    }

    sendXmlHttpRequest(getRelatedPostsURL(), JSON.stringify({ source: 'shoedinate', productCd: productCd }), function(htmlOfList) {
      var shoedinateTitle = document.getElementById('coordinateTitle');
      if (htmlOfList.trim() == '') {
        shoedinateRelated.style.display = 'none';
        if (shoedinateTitle != null) {
          shoedinateTitle.style.display = 'none';
        }
      } else {
        shoedinateRelated.style.display = 'block';
        if (shoedinateTitle != null) {
          shoedinateTitle.style.display = 'block';
        }
        var ulContainer = shoedinateRelated.getElementsByTagName('ul')[0];
        ulContainer.innerHTML = htmlOfList;
      }
    });
  }

  window.showMore = function(className, nextPage, productCd) {
    sendXmlHttpRequest(getRelatedPostsURL(), JSON.stringify({ source: 'shoedinate', productCd: productCd, p: nextPage }), function(htmlOfList) {
      var list = document.getElementById('coordinateImg');
      list.getElementsByClassName(className)[0].remove();
      list.innerHTML += htmlOfList;
    });
  }

  function MakeConversion() {
    initShoeCookie();

    purchaseConfirm();

    getLatestPosts();

    getRelatedPosts();
  }

  // Start
  MakeConversion();

}());

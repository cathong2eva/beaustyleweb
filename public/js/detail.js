// 横幅などの設定
var options = {
  maxWidth : 520, // #photo_container の max-width を指定(px)
  thumbMaxWidth : 125, // #thumbnail li の max-width を指定(px)
  thumbMinWidth : 80, // #thumbnail li の min-width を指定(px)
  fade : 500 // フェードアウトするスピードを指定
};
$(function() {
  // 変数を作る
  var thumbList = $('#thumbnail ul').find('a'),
      mainPhoto = $('#main_photo'),
      img = $('<img />');

  var imgLoad = $('#thumbnail ul li.current a').attr('data-img');
  $(':root').on('mousedown', '#main_photo', function() {
        var el = $(this),
            events = $._data(this, 'events');
        if (events && events.clickHold) {
            el.data(
                'clickHoldTimer',
                setTimeout(
                    function() {
                        el.trigger('clickHold')
                    },
                    el.data('clickHoldTimeout')
                )
            );
        }
  }).on('mouseup mouseleave', '#main_photo', function() {
    clearTimeout($(this).data('clickHoldTimer'));
    $('#main_photo img').attr('src', imgLoad);
  });
  $('#main_photo').data('clickHoldTimeout', 200); //Time to hold
  $('#main_photo').on('clickHold', function() {
    imgLoad = $('#thumbnail ul li.current a').attr('data-img');
    var loadId = $('#thumbnail ul li.current a').attr('data-id');
    var imgCur = $('#' + loadId).val();
    $('#main_photo img').attr('src', imgCur);
  });

  // 親ボックスと li 要素に max-width 指定
  $('#photo_container').css('maxWidth', options.maxWidth);

  // li 要素の横幅の計算と指定
  var liWidth = Math.floor((options.thumbMaxWidth / options.maxWidth) * 100);
  $('#thumbnail li').css({
    width : liWidth + '%',
    maxWidth : options.thumbMaxWidth,
    minWidth : options.thumbMinWidth
  });

  // 最初の画像の div#main_photo へ表示と current クラスを指定
  img = img.attr({
    src: $(thumbList[0]).attr('data-img'),
    alt: $(thumbList[0]).find('img').attr('alt')
  });
  mainPhoto.append(img);
  $('#thumbnail li:first').addClass('current');
  var imgThumb = $('#inp0').val();
  $('.img_slide_thumb').attr('src', imgThumb);

  // メイン画像を先に読み込んどく
  for(var i = 0; i < thumbList.length; i++){
    thumnImg = $('<img />').attr({
      src: $(thumbList[i]).attr('data-img'),
      alt: $(thumbList[i]).find('img').attr('alt')
    });
    $(thumbList[i]).html(thumnImg);
  }

  // サムネイルのクリックイベント
  $('#thumbnail ul li').on('click', function() {
    // img 要素を作り サムネイル画像からリンク・altの情報を取得・設定する
    var thisImg = $(this).find('a');
    var photo = $('<img />').attr({
      src: $(thisImg).attr('data-img'),
      alt: $(thisImg).find('img').attr('alt')
    });

    // div#main_photo へ 上で作った img 要素を挿入する
    mainPhoto.find('img').before(photo);

    // div#main_photo に先に表示されていた img 要素をフェードしながら非表示にし要素を消す、
    mainPhoto.find('img:not(:first)').stop(true, true).fadeOut(options.fade, function() {
      $(this).remove();
    });

    // 新しく表示した img の親 li へ .current を付け、
    // 他の li 要素についていた .current を削除する
    $(thisImg).parent().addClass('current').siblings().removeClass('current');

    // 画像の親要素を現在表示中の画像の高さへ変更する
    mainPhoto.height(photo.outerHeight());
    mainPhoto.find('img').height(photo.outerHeight());
    imgLoad = $('#thumbnail ul li.current a').attr('data-img');
    var inpImg = $(thisImg).attr('data-id');
    $('.img_slide_thumb').attr('src', $('#'+inpImg).val());
    return false;
  });

  // ウィンドウが読み込まれた時とリサイズされた時に
  // div#main_photo の高さを img の高さへ変更する
  $(window).on('resize load', function(){
    mainPhoto.height(mainPhoto.find('img').outerHeight());
    mainPhoto.find('img').height(mainPhoto.find('img').outerHeight());
  });

  // View ECSite
  $('.wrapper_item .order, .wrapper_item_smt .order_item').click(function(e) {
    var $me = $(this);

    $.ajax({
      type: 'POST',
      data: { id: $me.data('post-id') },
      dataType: 'json',
      url: '/viewECSite',
      success: function(data) {
        if (data.success) {
          if ($('.view_count_detail').length) {
            $('.view_count_detail').text(parseInt($('.view_count_detail').text()) + 1);
          }
        }
      }
    });
  });

  $('#show-hide-switcher').click(function(e) {
    var $icon = $(this).find('img');
    var $me = $(this);

    if ($icon.attr('data-status') === 'close') {
      $('#thumbnail ul').fadeOut(200, function() {
        $icon.attr('src', $me.attr('data-open'));
        $icon.attr('data-status', 'open');
      });
    } else {
      $icon.attr('src', $me.attr('data-close'));
      $icon.attr('data-status', 'close');
      $('#thumbnail ul').fadeIn(200);
    }
  });

});

$(function() {
  if ($('input.keyword-search').length) {
    $('input.keyword-search').searchbox();
  }

  $('.trend_search #navigation input[type=radio]').each(function(idx, radio) {
    if ($(radio).is(':checked')) {
      var $keywordSearch = $('input.keyword-search');

      $keywordSearch.attr('placeholder', $(radio).attr('placeholder'));
      $keywordSearch.prop('disabled', ($(radio).val() == 'height' ? true : false));
    }
  });

  $('form.search').submit(function() {
    var previousKeyword = $('.trend_search #navigation input[type=radio]:checked').data('keyword');

    if ($('input.keyword-search').val() != previousKeyword) {
      $('input.keyword-search').trigger('keyup');
    }

    return false;
  });

  $('.trend_search #navigation input[type=radio]').click(function() {
    var $keywordSearch = $('input.keyword-search');

    $keywordSearch.attr('placeholder', $(this).attr('placeholder'));
    $keywordSearch.prop('disabled', ($(this).val() == 'height'));

    if ($(this).val() == 'height') {
      $keywordSearch.val('');
    } else if ($keywordSearch.val() == '' && $keywordSearch.data('previous-keyword') != '') {
      $keywordSearch.val($keywordSearch.data('previous-keyword'));
    }

    if ($(this).is(':checked') && $(this).val() != 'height' && $keywordSearch.val() != $(this).data('keyword')) {
      $keywordSearch.trigger('keyup');
    }
  });

  $('.reset_button').click(function() {
    if ($('.trend_search #top').is(':checked') || $('.trend_search #shop').is(':checked')) {
      $('input.keyword-search').trigger('keyup');
    }
  });
});

(function($) {
  $.searchbox = {};

  $.extend(true, $.searchbox, {
    settings: {
      url: '/lookup',
      delay: 500
    },

    resetTimer: function(timer) {
      if (timer) {
        clearTimeout(timer);
      }
    },

    process: function(terms, type) {
      $.ajax({
        type: 'POST',
        data: { q: terms, type: type },
        dataType: 'html',
        url: '/lookup',
        beforeSend: function(jqXHR, settings) {
          if (type === 'height') {
            return false;
          }

          $('ul.content-' + type).html('');

          $('.navigation-content').loader({
            bgColor: '#eee',
            image: '/img/loading.gif',
            text: 'Waiting for search ...'
          });
        },
        success: function(data) {
          $('input#'+type).data('keyword', terms);
          $('input.keyword-search').data('previous-keyword', terms);
          $('ul.content-' + type).html(data);
        },
        complete: function() {
          $('.navigation-content').loader('hide');
        }
      });
    }
  });

  $.fn.searchbox = function(config) {
    var settings = $.extend(true, $.searchbox.settings, config || {});

    return this.each(function() {
      var $input = $(this);

      $input.keyup(function() {
        $.searchbox.resetTimer(this.timer);

        if ($input.val()) {
          this.timer = setTimeout(function() {
            $.searchbox.process($input.val(), $('.trend_search #navigation input[type=radio]:checked').val())
          }, $.searchbox.settings.delay);
        }
      });
    });
  }
})(jQuery);

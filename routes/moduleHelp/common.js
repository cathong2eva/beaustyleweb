var api = require('../../models/api.js').initialize();
var _ = require('underscore');

const LIMIT_ON_PAGE = 20;
const PREFIX_COOKIE = 'sdnt_ck_';
const LIMIT_ON_PAGE_DETAIL = 6;

getCookiesPosts = function(req) {
  var postIds = [];

  if (!_.isEmpty(req.cookies)) {
    var cookiesKeys = _.keys(req.cookies);

    for (var i = 0; i < cookiesKeys.length; i++) {
      if (cookiesKeys[i].startsWith(PREFIX_COOKIE)) {
        var postId = cookiesKeys[i].replace(PREFIX_COOKIE, '');
        postIds.push(postId);
      }
    }
  }

  return postIds;
};

loadData = function(req, res, next, options) {
  var page = (typeof req.params.p != 'undefined') ? parseInt(req.params.p) : 1;
  var offset = (page - 1) * LIMIT_ON_PAGE;
  var limit = LIMIT_ON_PAGE + 1;
  var nextPage = 0;
  var hasLayout = (req.xhr) ? false : true; // Ajax or Not
  var sort = (req.query.sort != undefined && req.query.sort < 5) ? parseInt(req.query.sort) : 1;
  var cookiesPostIds = getCookiesPosts(req);

  var currentUrl = '/' + options.path + '/';

  var postPromise;
  if (options.path == 'favorites') {
    postPromise = api.getPostsByIds(options.ids, offset, limit);
  } else if (options.path == 'popular-staffs') {
    postPromise = api.getPostPopularStaff(offset, limit);
  } else if (options.path == 'rankings') {
    if (parseInt(sort) == 4) {
      postPromise = api.getRankingPostMonthly(offset, limit);
    } else {
      postPromise = api.getRankingPost(sort, offset, limit);
    }
  } else {
    postPromise = api.getPostsStyle(options.orderBy, offset, limit);
  }

  var promises = [];
  // promises.push(api.getBannerActiveLists());
  // promises.push(api.getCountShowBanner());
  promises.push(postPromise);

  Promise.all(promises).then(function(result) {
    if (result[0].length > LIMIT_ON_PAGE) {
      result[0] = result[0].slice(0, LIMIT_ON_PAGE);
      nextPage = page + 1;
    } else {
      nextPage = 0;
    }

    res.render(
      (options.path != 'favorites') ? 'post/index' : 'post/favorites',
      {
        title: options.title,
        page: page,
        numBanner: 0,//result[1],
        ads: [],//result[0],
        posts: result[0],
        path: options.path,
        nextPage: nextPage,
        cookies: cookiesPostIds,
        numFavorite: _.size(cookiesPostIds),
        layout: hasLayout,
        currentUrl: currentUrl,
        sort: sort
        //arrImgs: arrImgs
      }
    );
  }, function(xhrObj) {
    showError(next);
  });
};

getPostsByStylist = function(req, res, next, options) {
    var page = (typeof req.params.p != 'undefined') ? parseInt(req.params.p) : 1;
    var offset = (page - 1) * LIMIT_ON_PAGE;
    var limit = LIMIT_ON_PAGE_DETAIL + 1;
    var nextPage = 0;
    var hasLayout = (req.xhr) ? false : true; // Ajax or Not
    var sort = (req.query.sort != undefined && req.query.sort < 5) ? parseInt(req.query.sort) : 1;
    var cookiesPostIds = getCookiesPosts(req);
    var currentUrl = '/' + options.path + '/' + options.id + '/';
    var postPromise;
    postPromise = api.getPostsByUserId(options.id, sort, offset, limit);
    var promises = [];
    promises.push(postPromise);
    promises.push(api.getUserInfo(options.id));

    Promise.all(promises).then(function(result) {
        if (result[0].length > LIMIT_ON_PAGE_DETAIL) {
            result[0] = result[0].slice(0, LIMIT_ON_PAGE_DETAIL);
            nextPage = page + 1;
        } else {
            nextPage = 0;
        }

        res.render(
            'post/lists',
            {
                title: options.title,
                page: page,
                posts: result[0],
                infoUser: result[1],
                path: options.path,
                nextPage: nextPage,
                cookies: cookiesPostIds,
                numFavorite: _.size(cookiesPostIds),
                layout: hasLayout,
                currentUrl: currentUrl,
                sort: sort
            }
        );
    }, function(xhrObj) {
        showError(next);
    });
};

getListOfPosts = function(req, res, next, opts) {
  var page = (typeof req.params.p != 'undefined') ? parseInt(req.params.p) : 1;
  var offset = (page - 1) * LIMIT_ON_PAGE;
  var limit = LIMIT_ON_PAGE + 1;
  var sort = (req.query.sort != undefined && req.query.sort < 5) ? parseInt(req.query.sort) : 1;
  var nextPage = 0;
  var hasLayout = (req.xhr) ? false : true; // Ajax or Not
  var cookiesPostIds = getCookiesPosts(req);

  var currentUrl = '/' + opts.action + '/' + opts.id + '/';

  var promise = Promise.resolve([]);
  switch (opts.action) {
    case 'staff':
      promise = api.getPostsByStaffId(opts.id, sort, offset, limit);
      break;
    case 'shop':
      promise = api.getPostsByShopId(opts.id, sort, offset, limit);
      break;
    case 'height':
      promise = api.getPostsByHeights(opts.range, sort, offset, limit);
      break;
  }

  promise.then(function(results) {
    if (results.length > LIMIT_ON_PAGE) {
      results = results.slice(0, LIMIT_ON_PAGE);
      nextPage = page + 1;
    }

    var responseObj = { title: opts.title, path: '', 'posts': results, cookies: cookiesPostIds, numFavorite: _.size(cookiesPostIds), 'sort': sort, currentUrl: currentUrl, nextPage: nextPage, layout: hasLayout, total: 0 };
    if (hasLayout) {
      var countPostPromise = Promise.resolve(0);
      switch (opts.action) {
        case 'staff':
          countPostPromise = api.countPostsByStaffId(opts.id, sort);
          break;
        case 'shop':
          countPostPromise = api.countPostsByShopId(opts.id, sort);
          break;
        case 'height':
          countPostPromise = api.countPostsByHeight(opts.range, sort);
          break;
      }

      countPostPromise.then(function(result) {
        if (opts.action == 'height') {
          responseObj.total = result;
        } else {
          if (result.param != '') {
            responseObj.title = result.param;

            // Override keywords of SEO
            res.locals.siteInfo.keywords = result.param;
          }
          responseObj.total = result.total;
        }

        res.render('search/list', responseObj);
      });
    } else {
      res.render('search/list', responseObj);
    }
  }, function(err) {
    showError(next);
  });
};

getItemsPostByCode = function(req, res, next) {
  var page = (typeof req.params.p != 'undefined') ? parseInt(req.params.p) : 1;
  var offset = (page - 1) * LIMIT_ON_PAGE;
  var limit = LIMIT_ON_PAGE + 1;
  var nextPage = 0;
  var name = (typeof req.query.name != 'undefined') ? req.query.name : '';
  var partNumber = (typeof req.query.partNumber != 'undefined') ? req.query.partNumber : '';
  var categoryCode = (typeof req.query.categoryCode != 'undefined') ? req.query.categoryCode : '';
  var sort = (req.query.sort != undefined && req.query.sort < 5) ? parseInt(req.query.sort) : 1;
  var hasLayout = (req.xhr) ? false : true;
  var cookiesPostIds = getCookiesPosts(req);
  var currentUrl = '/item';
  var queryString = '?name=' + name + '&partNumber=' + partNumber + '&categoryCode=' + categoryCode;

  var promises = [];

  promises.push(api.getItemByCode(name, partNumber, categoryCode, offset, limit));
  promises.push(api.getCountItemByCode(name, partNumber, categoryCode));

  Promise.all(promises).then(function(results) {
    if (results[0].length > LIMIT_ON_PAGE) {
      results[0] = results[0].slice(0, LIMIT_ON_PAGE);
      nextPage = page + 1;
    }
    var responseObj = {
      name: name,
      partNumber: partNumber,
      categoryCode: categoryCode,
      title: 'ITEMs',
      path: 'new',
      posts: results[0],
      cookies: cookiesPostIds,
      queryString: queryString,
      numFavorite: _.size(cookiesPostIds),
      currentUrl: currentUrl,
      nextPage: nextPage,
      layout: hasLayout,
      total: results[1]
    };

    res.render('search/list', responseObj);
  }, function() {
    showError(next);
  });
};

module.exports = {
  loadData: loadData,
  getCookiesPosts: getCookiesPosts,
  getListOfPosts: getListOfPosts,
  getItemsPostByCode: getItemsPostByCode,
  getPostsByStylist: getPostsByStylist
};

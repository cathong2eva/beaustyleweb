showError = function (next, message) {
  message = (typeof message != 'undefined') ? message : 'Page Not Found';

  var err = new Error(message);
  err.status = 404;
  next(err);
}

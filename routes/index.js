var express = require('express');
var router = express.Router();
var _ = require('underscore');
var loadData = require('./moduleHelp/common.js').loadData;
var getCookiesPosts = require('./moduleHelp/common.js').getCookiesPosts;
var api = require('../models/api.js').initialize();
var search = require('../models/search.js').initialize();
var getListOfPosts = require('./moduleHelp/common.js').getListOfPosts;
var getPostsByStylist = require('./moduleHelp/common.js').getPostsByStylist;
require('./moduleHelp/global.js');

const LIMIT_ON_PAGE = 20;
const NUMBER_ITEMS_RECOMMEND = 30;
const PREFIX_COOKIE = 'sdnt_ck_';
const description = 'description default';

// CORS on ExpressJS
// route middleware that will happen on every request
router.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

  res.locals.LIMIT_ON_PAGE = LIMIT_ON_PAGE;

  // Optimize for SEO
  res.locals.siteInfo = {
    keywords: 'パンプス,サンダル,エスペランサ,ESPERANZA,靴,SHOEDINATE,コーディネート,COORDINATE,SNAP',
    description: description,
    ogp: {
      title: '（エスペランサ）| ショップスタッフコーディネート - シューディネイト',
      image: 'http://shoedinate.com/img/thumb.jpg',
      url: 'http://shoedinate.com'
    }
  };

  next();
});

/* GET home page. */
router.get('/', function(req, res, next) {
  loadData(req, res, next, { title: 'New', 'path': 'new', orderBy: 'createdAt' });
});

router.get('/new/:p?', function(req, res, next) {
  loadData(req, res, next, { title: 'New', 'path': 'new', orderBy: 'createdAt' });
});

router.get('/stylist/:userId/:p?', function(req, res, next) {
    if (req.params.userId != undefined && req.params.userId) {
        getPostsByStylist(req, res, next, { title: "Profile", path: 'stylist', id: req.params.userId });
    } else {
        showError(next);
    }
});

router.get('/detail/:path/:postId', function(req, res, next) {
  var path = typeof req.params.path != 'undefined' ? req.params.path : 'view';
  var postId = typeof req.params.postId != 'undefined' ? req.params.postId : '';
  var paths = {
    'view': 'view',
    'rankings': 'ランキング',
    'popular-staffs': '人気スタッフ',
    'favorites': 'お気に入り一覧',
    'video': 'video',
    'lists': 'lists'
  };

  if (postId == '' || _.keys(paths).indexOf(path) < 0) {
    showError(next);
  } else {
    var cookiesPostIds = getCookiesPosts(req);
    var title = paths[path];
    var updateViewCount = [];
    var promises = [];

    // updateViewCount.push(api.updatePost(postId, 'viewCount', true));
    // updateViewCount.push(api.addTimeLine(postId, 'VIEW'));

    switch (path) {
      case 'view':
        promises.push(api.getPost(postId));
        break;
      case 'lists':
        promises.push(api.getUserInfo(postId));
        break;
    }

    Promise.all(promises).then(function(results) {  // Update PageView (PV)
      var promise = [];
      var post = results[0]; console.log(post.toJSON());
      if (typeof post != 'undefined') {
        var info = post.toJSON();
        if (path === 'view')
          promise.push(api.getRelatedPosts(postId, info.createdById));
        else if (path === 'lists')
          promise.push(api.getRelatedPosts(postId, info.createdById));

        Promise.all(promise).then(function(results) {
          //console.log(results[0]);
          var responseObj = {
              title: title,
              post: post,
              //tags: results[1],
              path: path,
              cookies: cookiesPostIds,
              numFavorite: _.size(cookiesPostIds),
              related: results[0]
            };
          res.render('post/' + path, responseObj);
        })
      } else {
        showError(next, 'Item is not exist.');
      }
      // promises.push(api.getPost(postId));
      // promises.push(search.getListOfTopTags(NUMBER_ITEMS_RECOMMEND));
      /*
      Promise.all(promises).then(function(results) {
        var post = results[0];

        if (typeof post != 'undefined') {
          var responseObj = {
            title: title,
            post: post,
            related: [],
            tags: results[1],
            path: path,
            cookies: cookiesPostIds,
            numFavorite: _.size(cookiesPostIds)
          };
          var ecLinked = post.get('ecLinked');
          var fixedTags = _.values(post.get('fixedTag')).filter(function(v){
            return (v !== (undefined || ''));
          });

          var keywordTags = fixedTags.join(',');
          if (typeof post.get('hashtags') != 'undefined') {
            keywordTags += post.get('hashtags').replace(/[# ]+/g, ',');
          }

          if (typeof ecLinked != 'undefined') {
            var subPostPromise = api.getRelatedPosts(postId, ecLinked.partNumber);

            res.locals.siteInfo = _.extend(res.locals.siteInfo, {
              keywords: keywordTags,
              description: (typeof post.get('caption') != 'undefined') ? post.get('caption') : ecLinked.name,
              ogp: {
                title: ecLinked.name,
                image: post.get('coordinate').url(),
                url: req.protocol + '://' + req.get('host') + req.originalUrl
              }
            });

            subPostPromise.then(function(relatedPosts) {
              responseObj.related = relatedPosts;

              res.render('post/view', responseObj);
            }, function(err) {
              res.render('post/view', responseObj);
            });
          } else {
            res.render('post/view', responseObj);
          }
        } else {
          showError(next, 'Item is not exist.');
        }
      }, function(xhrObj) {
        showError(next);
      });
      */


    }, function(err) {
      showError(next);
    });
  }
});

router.post('/like', function(req, res, next) {
  // Not Ajax request
  if (!req.xhr || typeof req.body.id == 'undefined' || req.body.id == '') {
    showError(next);
  } else {
    var postId = req.body.id;
    var point = typeof req.body.point != 'undefined' ? parseInt(req.body.point) : 1;
    var cookieKey = PREFIX_COOKIE + postId;

    if (typeof req.cookies[cookieKey] == 'undefined' || req.cookies[cookieKey] == 1) {
      if ((req.cookies[cookieKey] != 1 && point == 1) || (req.cookies[cookieKey] == 1 && point == -1)) {
        var postPromise = api.updatePost(postId, 'favoriteCount', (point > 0));
        var ip = (req.headers['x-forwarded-for']) ? req.headers['x-forwarded-for'] : req.connection.remoteAddress;

        postPromise.then(function(result) {
          if (point > 0) {
            var addTimeLine = api.addTimeLine(postId, 'LIKE', ip);
            addTimeLine.then(function(response) {
              res.cookie(cookieKey, 1, { expires: new Date(Date.now() + (7*24*60*60*1000)) });
              res.send({ 'success': true, 'message': 'Update success.' });
            }, function(err) {
              res.send({ 'success': false, 'message': 'Can not update.' });
            });
          } else {
            var removeTimeline = api.removeTimeline(postId, 'LIKE', ip);
            removeTimeline.then(function(response) {
              res.clearCookie(cookieKey);
              res.send({ 'success': true, 'message': 'Update success.' });
            }, function(err) {
              res.send({ 'success': false, 'message': 'Can not update.' });
            });
          }
        }, function(xhrObj) {
          res.send({ 'success': false, 'message': 'Can not update.' });
        });
      } else {
        res.send({ 'success': false, 'message': 'Invalid request.' });
      }
    } else {
      res.send({ 'success': false, 'message': 'Invalid request.' });
    }
  }
});

router.post('/viewECSite', function(req, res, next) {
  // Not Ajax request
  if (!req.xhr || typeof req.body.id == 'undefined' || req.body.id == '') {
    showError(next);
  } else {
    var postPromise = [];
    postPromise.push(api.updatePost(req.body.id, 'accessCount'));
    postPromise.push(api.addTimeLine(req.body.id, 'ACCESSCOUNT'));

    Promise.all(postPromise).then(function(result) {
      res.send({ 'success': true, 'message': 'Access to EC site successfully.' });
    }, function(xhrObj) {
      res.send({ 'success': false, 'message': 'Can not update access to EC site.' });
    });
  }
});

router.post('/conversion', function(req, res, next) {
  // Not Ajax request
  if (!req.xhr || typeof req.body.id == 'undefined' || req.body.id == '' ||
    typeof req.body.source == 'undefined' || req.body.source != 'shoedinate'
  ) {
    showError(next);
  } else {
    var postPromise = [];
    postPromise.push(api.updatePost(req.body.id, 'conversionCount'));
    postPromise.push(api.addTimeLine(req.body.id, 'CONVERSION'));

    Promise.all(postPromise).then(function(result) {
      res.send({ 'success': true, 'message': 'Make a conversion success.' });
    }, function(xhrObj) {
      res.send({ 'success': false, 'message': 'Can not update conversion.' });
    });
  }
});

router.get('/favorites/:p?', function(req, res, next) {
  var cookiesPostIds = getCookiesPosts(req);

  if (!_.isEmpty(cookiesPostIds)) {
    loadData(req, res, next, { title: 'FAVORITES', path: 'favorites', ids: cookiesPostIds, orderBy: '' });
  } else {
    res.redirect('/');
  }
});

router.get('/search', function(req, res, next) {
  var cookiesPostIds = getCookiesPosts(req);
  var promises = [];
  promises.push(search.getListOfTopTags(NUMBER_ITEMS_RECOMMEND)); // Get top 30 tags
  promises.push(search.search('', NUMBER_ITEMS_RECOMMEND, 'shop'));

  Promise.all(promises).then(function(results) {
    res.render('search/search', {
      title: '検索する',
      path: '',
      topTags: results[0],
      topShops: results[1],
      cookies: cookiesPostIds,
      numFavorite: _.size(cookiesPostIds)
    });
  }, function() {
    showError(next);
  });
});

router.post('/lookup', function(req, res, next) {
  // Not Ajax request
  if (!req.xhr) {
    showError(next);
  } else {
    var keyword = req.body.q;
    var type = req.body.type;

    search.search(keyword, NUMBER_ITEMS_RECOMMEND, type).then(function(results) {
      res.render('search/ajax', { type: type, data: results });
    });
  }
});

router.get('/tag/:hashtag/:p?', function(req, res, next) {
  if (req.params.hashtag != undefined && req.params.hashtag) {
    var tags = req.params.hashtag;
    var convertTags = (typeof tags == 'string') ? [tags] : tags;
    var sort = (req.query.sort != undefined && req.query.sort < 5) ? parseInt(req.query.sort) : 1;
    var page = (typeof req.params.p != 'undefined') ? parseInt(req.params.p) : 1;
    var offset = (page - 1) * LIMIT_ON_PAGE;
    var limit = LIMIT_ON_PAGE + 1;
    var nextPage = 0;
    var hasLayout = (req.xhr) ? false : true; // Ajax or Not
    var cookiesPostIds = getCookiesPosts(req);

    var postPromise = search.getRankingTags(convertTags, sort, limit, offset);

    var currentUrl = '/tag/' + tags;

    // Override keywords of SEO
    res.locals.siteInfo.keywords = tags;

    postPromise.then(function(results) {
      if (results.length > LIMIT_ON_PAGE) {
        results = results.slice(0, LIMIT_ON_PAGE);
        nextPage = page + 1;
      }

      var responseObj = { title: convertTags.join('-'),
        headerPage: '検索結果',
        path: '',
        'tags': convertTags,
        'sort': sort,
        'posts': results,
        cookies: cookiesPostIds,
        numFavorite: _.size(cookiesPostIds),
        currentUrl: currentUrl,
        nextPage: nextPage,
        layout: hasLayout,
        total: 0
      };

      if (hasLayout) {
        var countPostPromise = search.countTag(convertTags, sort);

        countPostPromise.then(function(num) {
          responseObj.total = num;
          res.render('search/result', responseObj);
        });
      } else {
        res.render('search/result', responseObj);
      }
    }, function() {
      showError(next);
    });
  } else {
    res.redirect('search/search');
  }
});

router.get('/shop/:shopId/:p?', function(req, res, next) {
  if (req.params.shopId != undefined && req.params.shopId) {
    getListOfPosts(req, res, next, { title: "SHOP's ITEMs", action: 'shop', id: req.params.shopId });
  } else {
    showError(next);
  }
});

router.get('/staff/:userId/:p?', function(req, res, next) {
  if (req.params.userId != undefined && req.params.userId) {
    getListOfPosts(req, res, next, { title: "STAFF's ITEMs", action: 'staff', id: req.params.userId });
  } else {
    showError(next);
  }
});

router.get('/height/:range/:p?', function(req, res, next) {
  if (req.params.range != undefined && req.params.range) {
    var rangeStr = req.params.range;
    var range = [];
    var title = "HEIGHT's ITEMs";

    if (rangeStr.indexOf('-') > -1) {
      var rangeArr = rangeStr.split('-');
      if (rangeArr.length == 2) {
        title = rangeArr[0] + 'センチ 〜 ' + rangeArr[1] + 'センチ';
        for (var i = parseInt(rangeArr[0]); i <= parseInt(rangeArr[1]); i++) {
          range.push(i + 'センチ');
        }
      }
    } else {
      var h = parseInt(rangeStr);
      if (h == 130) {
        title = '130センチ以下';
        range.push('130センチ以下');
      } else if (h == 180) {
        title = '180センチ以上';
        range.push('180センチ以上');
      }
    }

    if (range.length == 0) {
      showError(next);
    } else {
      // Override keywords of SEO
      res.locals.siteInfo.keywords = title.replace(' 〜 ', ',');

      getListOfPosts(req, res, next, { title: title, action: 'height', id: req.params.range, range: range });
    }
  } else {
    showError(next);
  }
});

router.get('/test', function(req, res, next) {
  res.render('test', { title: 'TEST', path: '', numFavorite: 0 });
});

router.get('/order/complete', function(req, res, next) {
  res.render('purchaseConfirm', { title: 'GiftShDispPurchConfirm', path: '', numFavorite: 0 });
});

router.get('/download', function(req, res, next) {
  var cookiesPostIds = getCookiesPosts(req);
  api.getVersion().then(function(version) {
    res.render('download', {
      title: 'ここからアプリをダウンロードしてください。',
      path: '', numFavorite: _.size(cookiesPostIds),
      appVersion: !_.isUndefined(version) ? version : ''
    });
  });
});

/* GENERATE LIST OF PRODUCTS FOR ECSite */
router.post('/posts/latest', function(req, res, next) {
  // Not Ajax request
  if ((!req.xhr) ||
    (typeof req.body.source == 'undefined' || req.body.source != 'shoedinate') ||
    (typeof req.body.sp == 'undefined')
  ) {
    showError(next);
  } else {
    var sp = parseInt(req.body.sp);
    if (sp == true) {
      var view = 'ec-site/ecPhone';
      var limit = 6;
    } else {
      var view = 'ec-site/ec';
      var limit = 20;
    }

    api.getPosts('createdAt', 0, limit).then(function(posts) {
      res.render(view, { data: posts});
    }, function(err) {
      res.send('');
    });
  }
});

router.post('/posts/related', function(req, res, next) {
  // Not Ajax request
  if ((!req.xhr) || typeof req.body.source == 'undefined' || req.body.source != 'shoedinate' || typeof req.body.productCd == 'undefined') {
    showError(next);
  } else {
    var productCds = req.body.productCd.split('-'); // productCd format: xxx-xxxx-xxxx
    var categoryCode = (productCds.length == 3) ? productCds[0] : '';
    var manufacturerCode = (productCds.length == 3) ? productCds[1] : '';
    var partNumber = (productCds.length == 3) ? productCds[2] : '';
    var page = (typeof req.body.p == 'undefined') ? 1 : parseInt(req.body.p);
    var LIMIT_COORDINATE = 5;
    var limit = LIMIT_COORDINATE + 1;
    var offset = (page - 1) * LIMIT_COORDINATE;
    var nextPage = 0;

    api.getRelatedPostsByPartNumber(categoryCode, manufacturerCode, partNumber, offset, limit).then(function(posts) {
      if (posts.length > LIMIT_COORDINATE) {
        posts = posts.slice(0, LIMIT_COORDINATE);
        nextPage = page + 1;
      }

      res.render('ec-site/ecDetails', { data: posts, nextPage: nextPage, productCd: req.body.productCd});
    }, function(err) {
      res.send('');
    });
  }
});

router.post('/updateViewBanner', function(req, res, next) {
  // Not Ajax request
  if (!req.xhr || typeof req.body.id == 'undefined' || req.body.id == '') {
    showError(next);
  } else {
    var bannerId = req.body.id;
    api.updateViewBanner(bannerId).then(function(results) {
      res.send({ 'success': true, 'message': 'Update success.' });
      }, function(xhrObj) {
      res.send({ 'success': false, 'message': 'Can not update.' });
    });
  }
});

router.get('/item:p?', function(req, res, next) {
  getItemsPostByCode(req, res, next);
});

module.exports = router;

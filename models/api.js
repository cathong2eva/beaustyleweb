(function() {
  var parseConfig = require('../config.js').parse;
  var Parse = require('parse/node');
  var _ = require('underscore');
  var UTCDateHelper = require('../helpers/utcDate.js');
  var MAX_QUERY_SIZE = 500;
  var moment = require('moment');

  module.exports = {
    /**
     * Initialize.
     */
    initialize: function() {
      // SDK connect to ShoeDinate API Server
      Parse.initialize(parseConfig.APP_ID, parseConfig.JAVASCRIPT_KEY);
      Parse.serverURL = parseConfig.SERVER_URL;

      return this;
    },

    getPostQuery: function() {
      var query = new Parse.Query('Post');
      //query.equalTo('status', 'Approved');

      getPostQuery = function() { return query; }

      return query;
    },

    getUserQuery: function() {
      var query = new Parse.Query('User');

      getUserQuery = function() { return query; }

      return query;
    },

    getShopQuery: function() {
      var query = new Parse.Query('Shop');

      getShopQuery = function() { return query; }

      return query;
    },

    getBannerQuery: function() {
      var query = new Parse.Query('Banner');

      getBannerQuery = function() { return query; };

      return query;
    },

    getTimeLineQuery: function() {
      var query = new Parse.Query('TimeLine');

      getTimeLineQuery = function() { return query; };

      return query;
    },

    getPostStatisticMonthQuery: function() {
      var query = new Parse.Query('StatisticsMonth');

      getPostStatisticMonthQuery = function() { return query; };

      return query;
    },


    getPostsStyle: function(descending, offset, limit) {
      var orderBy = (typeof descending !== 'undefined') ? descending : 'createdAt';
      var limitNumber = (typeof limit !== 'undefined') ? parseInt(limit) : 20;
      var offset = (typeof offset !== 'undefined') ? parseInt(offset) : 0;
      var postQuery = new Parse.Query('Post');
        //postQuery.equalTo('status', 'ACTIVE');
        postQuery.include('imagesAfter, imagesBefore');
        postQuery.skip(offset);
        postQuery.limit(limitNumber);
        postQuery.descending(orderBy);

        return postQuery.find().then(function(posts) {
            return Parse.Promise.resolve(posts);
        }, function(err) {
            return Parse.Promise.reject(err);
        });
    },


    /**
     * Get posts.
     * @param {String} descending
     * @param {Number} offset
     * @param {Number} limit
     * @return {Object}
     */
    getPosts: function(descending, offset, limit) {
      var orderBy = (typeof descending !== 'undefined') ? descending : 'createdAt';
      var limitNumber = (typeof limit !== 'undefined') ? parseInt(limit) : 20;
      var offset = (typeof offset !== 'undefined') ? parseInt(offset) : 0;

      var postQuery = new Parse.Query('Post');
      postQuery.equalTo('status', 'Approved');
      postQuery.include('createdBy.shop');
      postQuery.skip(offset);
      postQuery.limit(limitNumber);
      postQuery.descending(orderBy);

      return postQuery.find().then(function(posts) {
        return Parse.Promise.resolve(posts);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    getPostsByUserId: function(userId, sort, offset, limit) {
      var limitNumber = (typeof limit !== 'undefined') ? parseInt(limit) : 6;
      var offset = (typeof offset !== 'undefined') ? parseInt(offset) : 0;
      var postQuery = new Parse.Query('Post');
      //postQuery.equalTo('status', 'ACTIVE');
      postQuery.equalTo('createdById', userId);
      postQuery.include('imagesAfter, imagesBefore');
      postQuery.skip(offset);
      postQuery.limit(limitNumber);
      postQuery.descending('createdAt');

      return postQuery.find().then(function(posts) {
        return Parse.Promise.resolve(posts);
      }, function(err) {
        return Parse.Promise.reject(err);
      });

    },

      /**
     * getPostsByRankings
     *
     * @param {Number} sort
     * @param {Number} offset
     * @param {Number} limit
     * @return {Object}
     */

    getRankingPost: function(sort, offset, limit, postOfStaffQuery) {
      if (sort == 1) {
        if (!_.isUndefined(postOfStaffQuery) && !_.isEmpty(postOfStaffQuery)) {
          var postQuery = postOfStaffQuery;
          postQuery.descending('createdAt');
        } else {
          var postQuery = this.getPostQuery();
          postQuery.descending('viewCount');
        }
        postQuery.equalTo('status', 'Approved');
        postQuery.include('createdBy.shop');
        postQuery.skip(offset);
        postQuery.limit(limit);

        return postQuery.find().then(function(posts) {
          return Parse.Promise.resolve(posts);
        }, function(err) {
          return Parse.Promise.reject(err);
        });
      } else {
        var currentMonth = moment();
        var startDate = '';
        var endDate = '';

        switch (sort) {
          case 2: // Yesterday
            startDate = {'__type': 'Date', 'iso': currentMonth.subtract(1, 'day').format('YYYY-MM-DD') + 'T00:00:00.000Z'};
            endDate = {'__type': 'Date', 'iso': currentMonth.format('YYYY-MM-DD') + 'T23:59:59.000Z'};
            break;
          case 3: // Last week
            startDate = {'__type': 'Date', 'iso': currentMonth.subtract(1, 'weeks').startOf('isoWeek').format('YYYY-MM-DD') + 'T00:00:00.000Z'};
            endDate = {'__type': 'Date', 'iso': currentMonth.endOf('isoWeek').format('YYYY-MM-DD') + 'T23:59:59.000Z'};
            if (_.isUndefined(postOfStaffQuery) || _.isEmpty(postOfStaffQuery)) {
              var postOfStaffQuery = this.getPostQuery();
              postOfStaffQuery.greaterThanOrEqualTo('approvedAt', startDate);
              postOfStaffQuery.lessThanOrEqualTo('approvedAt', endDate);
              postOfStaffQuery.equalTo('status', 'Approved');
            }
            break;
          // case 4: // This month
          //   startDate = {'__type': 'Date', 'iso': currentMonth.startOf('month').format('YYYY-MM-DD') + 'T00:00:00.000Z'};
          //   endDate = {'__type': 'Date', 'iso': currentMonth.endOf('month').format('YYYY-MM-DD') + 'T23:59:59.000Z'};
          //   break;
        }

        return this.getListPostLikedByTime(offset, limit, startDate, endDate, postOfStaffQuery).then(function(postList) {
          var postResults = [];
          for (var i = 0; i < postList.length; i++) {
            postResults.push(postList[i]['postObj']);
          }
          return Parse.Promise.resolve(postResults);
        }, function(err) {
          return Parse.Promise.reject(err);
        });
      }
    },

    getListPostLikedByTime: function(offset, limit, startDate, endDate, postOfStaffQuery) {
      return this.getAllLikedTimeLineByTime(startDate, endDate, 1, [], postOfStaffQuery).then(function(timeLine) {
        var post = {};
        for (var i = 0; i < timeLine.length; i++) {
          if (!_.isUndefined(timeLine[i].get('post'))) {
            var postId = timeLine[i].get('post').id;
            if (_.isUndefined(post[postId])) {
              post[postId] = {
                postObj: timeLine[i].get('post'),
                totalView: 1
              };
            } else {
              post[postId]['totalView'] += 1;
            }
          }
        }

        var listPost = _.sortBy(_.values(post), function(s) { return -parseInt(s.totalView); });
        listPost = listPost.slice(offset, offset + limit);
        return Parse.Promise.resolve(listPost);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    getAllLikedTimeLineByTime: function(startDate, endDate, page, timeLineObj, postOfStaffQuery) {
      var me = this;
      return me._getTimeLineByTime(startDate, endDate, page, postOfStaffQuery).then(function(timeLine) {
        if (timeLine.length > MAX_QUERY_SIZE) {
          timeLine = timeLine.slice(0, MAX_QUERY_SIZE);
          page = page + 1;
        } else {
          page = 0;
        }
        timeLineObj = _.union(timeLineObj, timeLine);
        return (page > 0)
          ? me.getAllLikedTimeLineByTime(startDate, endDate, page, timeLineObj, postOfStaffQuery)
          : Parse.Promise.resolve(timeLineObj);
      });
    },

    _getTimeLineByTime: function(startDate, endDate, page, postOfStaffQuery) {
      var timeLineQuery = this.getTimeLineQuery();

      if (!_.isUndefined(postOfStaffQuery) && !_.isEmpty(postOfStaffQuery)) {
        timeLineQuery.matchesQuery('post', postOfStaffQuery);
      }

      timeLineQuery.include('post');
      timeLineQuery.include('post.createdBy.shop');
      timeLineQuery.equalTo('type', 'VIEW');
      timeLineQuery.greaterThanOrEqualTo('createdAt', startDate);
      timeLineQuery.lessThanOrEqualTo('createdAt', endDate);
      timeLineQuery.skip(MAX_QUERY_SIZE * (page - 1));
      timeLineQuery.limit(MAX_QUERY_SIZE + 1);

      return timeLineQuery.find();
    },

    getPostPopularStaff: function(offset, limit) {
      var me = this;
      var group = ['Staff', 'Part-Time'];
      var limitNumber = (typeof limit !== 'undefined') ? parseInt(limit) : 20;
      var offset = (typeof offset !== 'undefined') ? parseInt(offset) : 0;

      var staffQuery = me.getUserQuery();
      staffQuery.containedIn('group', group);
      staffQuery.greaterThan('totalPost', 0);
      staffQuery.descending('totalView');
      staffQuery.skip(offset);
      staffQuery.limit(limitNumber);

      return staffQuery.find().then(function(staffList) {
        return me.getLatestPost(staffList).then(function(postList) {
          return Parse.Promise.resolve(postList);
        }, function(err) {
          return Parse.Promise.reject(err);
        });
      });
    },

    getLatestPost: function(staffList) {
      var promise = [];

      for (var i = 0; i < staffList.length; i++) {
        var postQuery = this.getPostQuery();
        postQuery.equalTo('createdBy', staffList[i]);
        postQuery.equalTo('status', 'Approved');
        postQuery.include('createdBy');
        postQuery.include('createdBy.shop');
        postQuery.descending('createdAt');
        promise.push(postQuery.first());
      }
      return Parse.Promise.when(promise).then(function(result) {
        return Parse.Promise.resolve(result);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    getPopularStaffPosts: function(descending, offset, limit) {
      var orderBy = (typeof descending !== 'undefined') ? descending : 'createdAt';
      var limitNumber = (typeof limit !== 'undefined') ? parseInt(limit) : 20;
      var offset = (typeof offset !== 'undefined') ? parseInt(offset) : 0;

      var rankingQuery = new Parse.Query('Ranking');
      rankingQuery.include('Post');
      rankingQuery.include('Post.createdBy.shop');
      rankingQuery.skip(offset);
      rankingQuery.limit(limitNumber);
      rankingQuery.ascending(orderBy);

      return rankingQuery.find().then(function(posts) {
        var results = [];
        for (var i = 0; i < posts.length; i++) {
          results.push(posts[i].get('Post'));
        }
        return Parse.Promise.resolve(results);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    getUserInfo: function(postId) {
      var postId = (typeof postId !== 'undefined') ? postId : '';

      var userQuery = this.getUserQuery();
      userQuery.include('profile', 'salon', 'avatar', 'salon.stylistId');
      userQuery.equalTo('objectId', postId);

      return userQuery.first().then(function(post) {
        return Parse.Promise.resolve(post);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    getPost: function(postId) {
      var postId = (typeof postId !== 'undefined') ? postId : '';

      var postQuery = this.getPostQuery();
      postQuery.include('createdBy', 'createdBy.salon', 'createdBy.avatar', 'imagesAfter', 'imagesBefore', 'menus');
      postQuery.equalTo('objectId', postId);

      return postQuery.first().then(function(post) {
        return Parse.Promise.resolve(post);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    getRelatedPosts: function(postId, user) {
      var postId = (typeof postId !== 'undefined') ? postId : '';
      var partNumber = (typeof partNumber !== 'undefined') ? partNumber : '';

      var postQuery = this.getPostQuery();
      postQuery.equalTo('createdById', user);
      postQuery.notEqualTo('objectId', postId);
      //postQuery.equalTo('status', 'ACTIVE');
      postQuery.limit(8);
      postQuery.descending('createdAt');
      postQuery.include('imagesAfter', 'imagesBefore');

      return postQuery.find().then(function(posts) {
        return Parse.Promise.resolve(posts);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    getRelatedPostsByPartNumber: function(categoryCode, manufacturerCode, partNumber, offset, limit) {
      var limitNumber = (typeof limit !== 'undefined') ? parseInt(limit) : 5;

      var postQuery = this.getPostQuery();
      postQuery.include('createdBy.shop');
      postQuery.equalTo('ecLinked.partNumber', partNumber);
      postQuery.equalTo('ecLinked.categoryCode', categoryCode);
      postQuery.equalTo('ecLinked.manufacturerCode', manufacturerCode);
      postQuery.descending('createdAt');
      postQuery.skip(offset);
      postQuery.limit(limitNumber);

      return postQuery.find();
    },


    /**
     * Update Post
     * @param {String} postId Post.objectId
     * @param {String} fieldName
     * @param {Boolean} isIncrement
     * @return {Parse.Promise}
     */
    updatePost: function(postId, fieldName, isIncrement) {
      var isIncrement = (typeof isIncrement != 'undefined') ? isIncrement : true;

      var postQuery = this.getPostQuery();
      return postQuery.get(postId).then(function(post) {
        post.increment(fieldName, (isIncrement ? 1 : -1));

        return post.save().then(function() {
          return Parse.Promise.resolve(true);
        }, function(err) {
          return Parse.Promise.reject(err);
        });
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    getPostsByStaffId: function(userId, sort, offset, limit) {
      var userQuery = this.getUserQuery();
      userQuery.equalTo('objectId', userId);

      var postOfStaff = Parse.Object.extend('Post');
      var postOfStaffQuery = new Parse.Query(postOfStaff);
      postOfStaffQuery.matchesQuery('createdBy', userQuery);

      return this.getRankingPost(sort, offset, limit, postOfStaffQuery);
    },

    getPostsByShopId: function(shopId, sort, offset, limit) {
      var shopPointer = new Parse.Object('Shop');
      shopPointer.id = shopId;

      var userQuery = this.getUserQuery();
      userQuery.equalTo('shop', shopPointer);

      var postOfStaff = Parse.Object.extend('Post');
      var postOfStaffQuery = new Parse.Query(postOfStaff);
      postOfStaffQuery.matchesQuery('createdBy', userQuery);

      return this.getRankingPost(sort, offset, limit, postOfStaffQuery);
    },

    getPostsByIds: function(postIds, offset, limit) {
      postIds = (typeof postIds !== 'undefined') ? postIds : [];

      var postQuery = this.getPostQuery();
      postQuery.include('createdBy.shop');
      postQuery.containedIn('objectId', postIds);
      postQuery.descending('favoriteCount');
      postQuery.skip(offset);
      postQuery.limit(limit);

      return postQuery.find().then(function(posts) {
        return Parse.Promise.resolve(posts);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    getPostsByHeights: function(rangeHeights, sort, offset, limit) {
      var userQuery = this.getUserQuery();
      userQuery.containedIn('height', rangeHeights);

      var postOfStaff = Parse.Object.extend('Post');
      var postOfStaffQuery = new Parse.Query(postOfStaff);
      postOfStaffQuery.matchesQuery('createdBy', userQuery);

      return this.getRankingPost(sort, offset, limit, postOfStaffQuery);
    },

    countPostsByHeight: function(rangeHeights, sort) {
      var userQuery = this.getUserQuery();
      userQuery.containedIn('height', rangeHeights);

      var postOfStaff = Parse.Object.extend('Post');
      var postOfStaffQuery = new Parse.Query(postOfStaff);
      postOfStaffQuery.matchesQuery('createdBy', userQuery);
      postOfStaffQuery.equalTo('status', 'Approved');

      return this.countPosts(postOfStaffQuery, sort).then(function(total) {
        return Parse.Promise.resolve(total);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    countPostsByStaffId: function(userId, sort) {
      var userQuery = this.getUserQuery();
      userQuery.equalTo('objectId', userId);

      var postOfStaff = Parse.Object.extend('Post');
      var postOfStaffQuery = new Parse.Query(postOfStaff);
      postOfStaffQuery.matchesQuery('createdBy', userQuery);
      postOfStaffQuery.equalTo('status', 'Approved');

      var promises = [];
      promises.push(this.countPosts(postOfStaffQuery, sort));
      promises.push(userQuery.first());

      return Parse.Promise.when(promises).then(function(results) {
        var staffName = '';
        if (typeof results[1] !== 'undefined') {
          staffName = results[1].get('nickname');
        }

        return Parse.Promise.resolve({ param: staffName, total: results[0] });
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    countPostsByShopId: function(shopId, sort) {
      var shopPointer = new Parse.Object('Shop');
      shopPointer.id = shopId;

      var userQuery = this.getUserQuery();
      userQuery.equalTo('shop', shopPointer);

      var postOfStaff = Parse.Object.extend('Post');
      var postOfStaffQuery = new Parse.Query(postOfStaff);
      postOfStaffQuery.matchesQuery('createdBy', userQuery);
      postOfStaffQuery.equalTo('status', 'Approved');

      var promises = [];
      promises.push(this.countPosts(postOfStaffQuery, sort));

      var shopQuery = this.getShopQuery();
      shopQuery.equalTo('objectId', shopId);

      promises.push(shopQuery.first());

      return Parse.Promise.when(promises).then(function(results) {
        var shopName = '';
        if (typeof results[1] !== 'undefined') {
          shopName = results[1].get('name');
          shopName += (results[1].get('salePerson') == 0) ? '' : '店';
        }

        return Parse.Promise.resolve({ param: shopName, total: results[0] });
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    countPosts: function(postOfStaffQuery, sort) {
      if (sort == 1) {
        var countQuery = postOfStaffQuery;

        return countQuery.count().then(function(num) {
          return Parse.Promise.resolve(num);
        }, function(err) {
          return Parse.Promise.reject(err);
        });
      } else {
        var currentMonth = moment();
        var startDate = '';
        var endDate = '';
        switch (sort) {
          case 2: // Yesterday
            startDate = {'__type': 'Date', 'iso': currentMonth.subtract(1, 'day').format('YYYY-MM-DD') + 'T00:00:00.000Z'};
            endDate = {'__type': 'Date', 'iso': currentMonth.format('YYYY-MM-DD') + 'T23:59:59.000Z'};
            break;
          case 3: // Last week
            startDate = {'__type': 'Date', 'iso': currentMonth.subtract(1, 'weeks').startOf('isoWeek').format('YYYY-MM-DD') + 'T00:00:00.000Z'};
            endDate = {'__type': 'Date', 'iso': currentMonth.endOf('isoWeek').format('YYYY-MM-DD') + 'T23:59:59.000Z'};
            break;
          case 4: // This month
            startDate = {'__type': 'Date', 'iso': currentMonth.startOf('month').format('YYYY-MM-DD') + 'T00:00:00.000Z'};
            endDate = {'__type': 'Date', 'iso': currentMonth.endOf('month').format('YYYY-MM-DD') + 'T23:59:59.000Z'};
            break;
        }

        return this.getAllLikedTimeLineByTime(startDate, endDate, 1, [], postOfStaffQuery).then(function(results) {
          var post = {};
          for (var i = 0; i < results.length; i++) {
            var postId = results[i].get('post').id;
            if (_.isUndefined(post[postId])) {
              post[postId] = {
                postObj: results[i].get('post'),
                totalLike: 1
              };
            } else {
              post[postId]['totalLike'] += 1;
            }
          }
          var listPost = _.allKeys(post).length;

          return Parse.Promise.resolve(listPost);
        }, function(err) {
          return Parse.Promise.reject(err);
        });
      }
    },

    getBannerActiveLists: function() {
      var bannerQuery = this.getBannerQuery();
      bannerQuery.equalTo('status', 'Active');
      bannerQuery.ascending('position');

      return bannerQuery.find().then(function(bannerObj) {
        return Parse.Promise.resolve(bannerObj);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    getCountShowBanner: function() {
      var bannerQuery = this.getBannerQuery();
      bannerQuery.equalTo('state', true);

      return bannerQuery.count().then(function(num) {
        return Parse.Promise.resolve(num);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    updateViewBanner: function(bannerId) {
      var bannerQuery = this.getBannerQuery();

      return bannerQuery.get(bannerId).then(function(banner) {
        var viewCount = 1;

        if (typeof banner.get('viewCount') != 'undefined' && banner.get('viewCount') != '') {
          viewCount = parseInt(banner.get('viewCount')) + 1;
        }

        banner.set('viewCount', viewCount);

        return banner.save(null).then(function(result) {
          return Parse.Promise.resolve(true);
        }, function(err) {
          return Parse.Promise.reject(err);
        });
      });
    },

    getVersion: function() {
      return Parse.Config.get().then(function(config) {
        return Parse.Promise.resolve(config.get('appVersion'));
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    addTimeLine: function(postId, type, ip) {
      var Post = Parse.Object.extend('Post');
      var postPointer = new Post;
      postPointer.id = postId;

      var TimeLine = Parse.Object.extend('TimeLine');
      var timeLineObj = new TimeLine();
      timeLineObj.set('post', postPointer);
      timeLineObj.set('type', type);
      timeLineObj.set('postId', postId);
      if (!_.isUndefined(ip) && !_.isEmpty(ip)) {
        timeLineObj.set('ip', ip);
      }

      return timeLineObj.save(null).then(function(result) {
        return Parse.Promise.resolve(true);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    removeTimeline: function(postId, type, ip) {
      var Post = Parse.Object.extend('Post');
      var postPointer = new Post;
      postPointer.id = postId;

      var timeLineQuery = this.getTimeLineQuery();
      timeLineQuery.equalTo('post', postPointer);
      timeLineQuery.equalTo('type', type);
      if (!_.isUndefined(ip) && !_.isEmpty(ip)) {
        timeLineQuery.equalTo('ip', ip);
      }

      return timeLineQuery.first().then(function(result) {
        return result.destroy().then(function(respone) {
          return Parse.Promise.resolve(true);
        }, function(err) {
          return Parse.Promise.reject(err);
        });
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    getItemByCode: function(name, partNumber, categoryCode, offset, limit) {
      var limitNumber = (typeof limit !== 'undefined') ? parseInt(limit) : 20;
      var offset = (typeof offset !== 'undefined') ? parseInt(offset) : 0;

      var postQuery = this.queryPostByCode(name, partNumber, categoryCode);
      postQuery.skip(parseInt(offset));
      postQuery.limit(limitNumber);

      return postQuery.find().then(function(results) {
        return Parse.Promise.resolve(results);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    getCountItemByCode: function(name, partNumber, categoryCode) {
      var postQuery = this.queryPostByCode(name, partNumber, categoryCode);

      return postQuery.count().then(function(count) {
        return Parse.Promise.resolve(count);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    queryPostByCode: function(name, partNumber, categoryCode) {
      var postQuery = this.getPostQuery();
      postQuery.include('createdBy.shop');
      postQuery.equalTo('ecLinked.partNumber', partNumber);
      postQuery.equalTo('ecLinked.categoryCode', categoryCode);
      postQuery.equalTo('ecLinked.name', name);
      postQuery.descending('createdAt');

      return postQuery
    },

    getRankingPostMonthly: function(offset, limit) {
      var month = moment().format('YYYY-MM');
      var postStatisticMonthQuery = this.getPostStatisticMonthQuery();
      postStatisticMonthQuery.exists('post');
      postStatisticMonthQuery.include(['post', 'post.createdBy', 'post.createdBy.shop']);
      postStatisticMonthQuery.equalTo('monthOfYear', month);
      postStatisticMonthQuery.descending('countView');
      postStatisticMonthQuery.skip(offset);
      postStatisticMonthQuery.limit(limit);

      return postStatisticMonthQuery.find().then((results) => {
        var posts = [];
        for (var i = 0; i < results.length; i++) {
          posts.push(results[i].get('post'));
        }

        return Parse.Promise.resolve(posts);
      }).catch((err) => {
        return Parse.Promise.reject(err);
      });
    }
  };
})();

(function() {
  var parseConfig = require('../config.js').parse;
  var Parse = require('parse/node');
  var _ = require('underscore');
  var UTCDateHelper = require('../helpers/utcDate.js');
  var api = require('../models/api.js').initialize();
  var moment = require('moment');

  module.exports = {
    initialize: function() {
      // SDK connect to ShoeDinate API Server
      Parse.initialize(parseConfig.APP_ID, parseConfig.JAVASCRIPT_KEY);
      Parse.serverURL = parseConfig.SERVER_URL;

      return this;
    },

    getPostQuery: function() {
      var query = new Parse.Query('Post');
      query.equalTo('status', 'Approved');

      getPostQuery = function() { return query; }

      return query;
    },

    getUserQuery: function() {
      var query = new Parse.Query('User');

      getUserQuery = function() { return query; }

      return query;
    },

    getShopQuery: function() {
      var query = new Parse.Query('Shop');

      getShopQuery = function() { return query; }

      return query;
    },

    countTag: function(tags, sort) {
      var postQuery = this.buildCompoundQueryTagsPost(tags);

      return api.countPosts(postQuery, sort).then(function(total) {
        return Parse.Promise.resolve(total);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    buildCompoundQueryTagsPost: function(tags) {
      // Convert each tag to lowercase
      tags = _.map(tags, function(tag) { return tag.toLowerCase(); });

      var tagsQuery = this.getPostQuery();
      tagsQuery.containedIn('tags', tags);

      // if (tags.length > 1) {
      //   var postQuery = tagsQuery;
      // } else {
      //   var strTag = tags[0].toString();

      //   var fixedTagBrandQuery = this.getPostQuery();
      //   fixedTagBrandQuery.matches('fixedTag.brand', new RegExp(strTag, 'i'));

      //   var fixedTagNicknameQuery = this.getPostQuery();
      //   fixedTagNicknameQuery.matches('fixedTag.nickname', new RegExp(strTag, 'i'));

      //   var fixedTagShopQuery = this.getPostQuery();
      //   fixedTagShopQuery.matches('fixedTag.shop', new RegExp(strTag, 'i'));

      //   var fixedTagCategoryQuery = this.getPostQuery();
      //   fixedTagCategoryQuery.matches('fixedTag.category', new RegExp(strTag, 'i'));

      //   var fixedTagHeightQuery = this.getPostQuery();
      //   fixedTagHeightQuery.matches('fixedTag.height', new RegExp(strTag, 'i'));

      //   var postQuery = Parse.Query.or(
      //     tagsQuery,
      //     fixedTagBrandQuery,
      //     fixedTagNicknameQuery,
      //     fixedTagShopQuery,
      //     fixedTagCategoryQuery,
      //     fixedTagHeightQuery
      //   );
      // }

      return tagsQuery;
    },

    getRankingTags: function(tags, sort, limit, offset) {
      var limitNumber = (typeof limit !== 'undefined') ? parseInt(limit) : 20;
      var offset = (typeof offset !== 'undefined') ? parseInt(offset) : 0;
      var postQuery = this.buildCompoundQueryTagsPost(tags);

      if (sort == 1) {
        postQuery.include('createdBy.shop');
        postQuery.descending('createdAt');
        postQuery.skip(offset);
        postQuery.limit(limitNumber);

        return postQuery.find().then(function(results) {
          return Parse.Promise.resolve(results);
        }, function(err) {
          return Parse.Promise.reject(err);
        });
      } else {
        var currentMonth = moment();
        var startDate = '';
        var endDate = '';
        switch (sort) {
          case 2: // Yesterday
            startDate = {'__type': 'Date', 'iso': currentMonth.subtract(1, 'day').format('YYYY-MM-DD') + 'T00:00:00.000Z'};
            endDate = {'__type': 'Date', 'iso': currentMonth.format('YYYY-MM-DD') + 'T23:59:59.000Z'};
            break;
          case 3: // Last week
            startDate = {'__type': 'Date', 'iso': currentMonth.subtract(1, 'weeks').startOf('isoWeek').format('YYYY-MM-DD') + 'T00:00:00.000Z'};
            endDate = {'__type': 'Date', 'iso': currentMonth.endOf('isoWeek').format('YYYY-MM-DD') + 'T23:59:59.000Z'};
            break;
          case 4: // This month
            startDate = {'__type': 'Date', 'iso': currentMonth.startOf('month').format('YYYY-MM-DD') + 'T00:00:00.000Z'};
            endDate = {'__type': 'Date', 'iso': currentMonth.endOf('month').format('YYYY-MM-DD') + 'T23:59:59.000Z'};
            break;
        }

        return api.getListPostLikedByTime(offset, limit, startDate, endDate, postQuery).then(function(results) {
          var postResults = [];
          for (var i = 0; i < results.length; i++) {
            postResults.push(results[i]['postObj']);
          }
          return Parse.Promise.resolve(postResults);
        }, function(err) {
          return Parse.Promise.reject(err);
        });
      }
    },

    /**
     * searchTags
     * @param {Array|String} tags
     * @param {Number} sort
     * @param {Number} limit
     * @param {Number} offset
     * @return {Object}
     */

    getListOfTopTags: function(limit) {
      return Parse.Cloud.run('lookupTags', {'keyword': '', 'limit': limit, 'web': 1 }, {
        success: function(results) {
          return Parse.Promise.resolve(results);
        },
        error: function(err) {
          return Parse.Promise.reject(err);
        }
      });
    },

    searchHashTags: function(keyword, limit) {
      return Parse.Cloud.run('lookupTags', { 'keyword': keyword, 'limit': limit, 'web': 1 }, {
        success: function(results) {
          return Parse.Promise.resolve(results);
        },
        error: function(err) {
          return Parse.Promise.reject(err);
        }
      });
    },

    searchItem: function(keyword, limit) {
      return Parse.Cloud.run('searchItemByMongoose', { 'keyword': keyword, 'limit': limit}, {
        success: function(results) {
          return Parse.Promise.resolve(results);
        },
        error: function(err) {
          return Parse.Promise.reject(err);
        }
      });
    },

    searchStaff: function(keyword, limit) {
      var staffNameQuery = this.getUserQuery();
      staffNameQuery.matches('name', new RegExp(keyword, 'i'));

      var staffKanaNameQuery = this.getUserQuery();
      staffKanaNameQuery.matches('kanaName', new RegExp(keyword, 'i'));

      var staffUsernameQuery = this.getUserQuery();
      staffUsernameQuery.matches('username', new RegExp(keyword, 'i'));

      var staffNicknameQuery = this.getUserQuery();
      staffNicknameQuery.matches('nickname', new RegExp(keyword, 'i'));

      var staffHeightQuery = this.getUserQuery();
      staffHeightQuery.matches('height', new RegExp(keyword, 'i'));

      // Compound Query
      var mainQuery = Parse.Query.or(
        staffNameQuery,
        staffKanaNameQuery,
        staffUsernameQuery,
        staffNicknameQuery,
        staffHeightQuery
      );
      mainQuery.containedIn('group', ['Staff', 'Part-Time']);
      mainQuery.descending(['totalConversion', 'totalFavorite', 'totalView']);
      mainQuery.limit(limit);

      return mainQuery.find().then(function(results) {
        return Parse.Promise.resolve(results);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    searchShop: function(keyword, limit) {
      var shopNameQuery = this.getShopQuery();
      shopNameQuery.matches('name', new RegExp(keyword, 'i'));

      var shopBrandQuery = this.getShopQuery();
      shopBrandQuery.matches('brand', new RegExp(keyword, 'i'));

      var shopStoreQuery = this.getShopQuery();
      shopStoreQuery.matches('storeName', new RegExp(keyword, 'i'));

      var shopAddressQuery = this.getShopQuery();
      shopAddressQuery.matches('address', new RegExp(keyword, 'i'));

      var mainQuery = Parse.Query.or(shopNameQuery, shopBrandQuery, shopStoreQuery, shopAddressQuery);
      mainQuery.descending('totalPost');
      mainQuery.limit(limit);

      return mainQuery.find().then(function(results) {
        return Parse.Promise.resolve(results);
      }, function(err) {
        return Parse.Promise.reject(err);
      });
    },

    search: function(keyword, limit, type) {
      if (type == 'top') {
        return this.searchHashTags(keyword, limit);
      } else {
        var promise;
        var regKeyword = new RegExp(keyword, 'i');
        if (type == 'item') {
          promise = this.searchItem(keyword, limit);

          return promise.then(function(results) {

            var responseObj = [];
            for (var i = 0; i < results.length; i++) {
              responseObj.push({
                name: results[i]['_id'],
                partNumber: results[i].partNumber[0],
                categoryCode: results[i].categoryCode[0],
                count: results[i].total,
                discription: results[i]['_id'] + ' (品番: ' + results[i].partNumber[0] + ', 分類: ' + results[i].categoryCode[0] + ')'
              });
            }
            return Parse.Promise.resolve(responseObj);
          });
        } else if (type == 'staff') {
          promise = this.searchStaff(keyword, limit);

          return promise.then(function(results) {
            var responseObj = [];
            var staffName = '';
            for (var i = 0; i < results.length; i++) {
              var cnt = (results[i].get('totalPost') != undefined) ? results[i].get('totalPost') : 0;

              if (cnt > 0) {
                staffName = results[i].get('name');
                if (staffName != undefined && staffName.match(regKeyword)) {
                  responseObj.push({ _id : results[i].id, match: staffName, count: cnt });
                } else if (results[i].get('kanaName') != undefined && results[i].get('kanaName').match(regKeyword)) {
                  responseObj.push({ _id : results[i].id, match: results[i].get('kanaName'), count: cnt });
                } else if (results[i].get('username') != undefined && results[i].get('username').match(regKeyword)) {
                  responseObj.push({ _id : results[i].id, match: staffName + ' (' + results[i].get('username') + ')', count: cnt });
                } else if (results[i].get('nickname') != undefined && results[i].get('nickname').match(regKeyword)) {
                  responseObj.push({ _id : results[i].id, match: staffName + ' (' + results[i].get('nickname') + ')', count: cnt });
                } else if (results[i].get('height') != undefined && results[i].get('height').match(regKeyword)) {
                  responseObj.push({ _id : results[i].id, match: staffName + ' (' + results[i].get('height') + ')', count: cnt });
                }
              }
            }

            return Parse.Promise.resolve(responseObj);
          });
        } else if (type == 'shop') {
          promise = this.searchShop(keyword, limit);

          return promise.then(function(results) {
            var responseObj = [];
            var shopName = '';
            for (var i = 0; i < results.length; i++) {
              var cnt = (results[i].get('totalPost') != undefined) ? results[i].get('totalPost') : 0;

              if (cnt > 0) {
                shopName = results[i].get('name');
                if (shopName != undefined && shopName.match(regKeyword)) {
                  responseObj.push({ _id : results[i].id, match: shopName, count: cnt });
                } else if (results[i].get('brand') != undefined && results[i].get('brand').match(regKeyword)) {
                  responseObj.push({ _id : results[i].id, match: shopName + ' (' + results[i].get('brand') + ')', count: cnt });
                } else if (results[i].get('storeName') != undefined && results[i].get('storeName').match(regKeyword)) {
                  responseObj.push({ _id : results[i].id, match: shopName + ' (' + results[i].get('storeName') + ')', count: cnt });
                } else if (results[i].get('address') != undefined && results[i].get('address').match(regKeyword)) {
                  responseObj.push({ _id : results[i].id, match: shopName + ' (' + results[i].get('address') + ')', count: cnt });
                }
              }
            }

            return Parse.Promise.resolve(responseObj);
          });
        }
      }
    }
  };
})();

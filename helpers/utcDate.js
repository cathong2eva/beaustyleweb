(function() {
  var currentDate = new Date(),
      year = currentDate.getFullYear(), // Returns the year
      month = currentDate.getMonth(), // Returns the month (from 0-11)
      dayOfMonth = currentDate.getDate(), // Returns the day of the month (from 1-31)
      dayOfWeek = currentDate.getDay(); // Returns the day of the week (from 0-6)

  module.exports = {
    version: '1.0.0',

    /**
     * getYesterday
     *
     * @return {Date}
     */
    getYesterday: function() {
      return new Date(Date.UTC(year, month, dayOfMonth - 1));
    },
    /**
     * getYesterdayEndTime
     *
     * @return {Date}
     */
    getYesterdayEndTime: function() {
      return new Date(Date.UTC(year, month, dayOfMonth - 1, 23, 59, 59, 999));
    },
    /**
     * getToday
     *
     * @return {Date}
     */
    getToday: function() {
      return new Date(Date.UTC(year, month, dayOfMonth));
    },
    /**
     * getTomorrow
     *
     * @return {Date}
     */
    getTomorrow: function() {
      return new Date(Date.UTC(year, month, dayOfMonth + 1));
    },
    /**
     * getFirstDateOfWeek
     *
     * @link http://ashok.press/how-to-get-start-date-of-a-week-in-javascript/
     * @return {Date}
     */
    getFirstDateOfWeek: function() {
      var firstDay = dayOfMonth - dayOfWeek + (dayOfWeek == 0 ? -6 : 1);
      return new Date(Date.UTC(year, month, firstDay));
    },
    /**
     * getLastDateOfWeek
     *
     * @link http://ashok.press/how-to-get-start-date-of-a-week-in-javascript/
     * @return {Date}
     */
    getLastDateOfWeek: function() {
      var lastDay = dayOfMonth - dayOfWeek + (dayOfWeek == 0 ? 0 : 7); // lastDay = firstDay + 6;
      return new Date(Date.UTC(year, month, lastDay, 23, 59, 59));
    },
    /**
     * getFirstDateOfMonth
     *
     * @return {Date}
     */
    getFirstDateOfMonth: function() {
      return new Date(Date.UTC(year, month, 1, 0, 0, 0));
    },
    /**
     * getLastDateOfMonth
     *
     * @return {Date}
     */
    getLastDateOfMonth: function() {
      return new Date(Date.UTC(year, month + 1, 0, 23, 59, 59));
    }
  };
}());
